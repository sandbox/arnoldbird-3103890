INTRODUCTION
---------------------

Ensures GUID uniqueness across all nodes, even if we use the 'Attach to content
type' feature.

This module is a workaround for this issue:
https://www.drupal.org/project/feeds/issues/1539224

To prevent non-unique nodes from being created, we throw an exception in
global_guid_checker_node_presave().  If you see a better (less of a hack)
way to achieve this, please file a bug report.


HOW TO USE
---------------------

Change the behavior by setting variables in your settings.php.  Here are the
variables and their defaults:

// The field name for your GUID.
$conf['global_guid_checker_guid_field'] = 'field_global_guid';

// Optionally, limit the uniqueness test to a specific bundle.
$conf['global_guid_checker_bundle'] = '';

// Change value to 0 if you don't want the module to trim excess Exception
// messages from the watchdog log.
$conf['global_guid_checker_watchdog_trim'] = 1;