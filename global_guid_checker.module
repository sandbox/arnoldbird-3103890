<?php

/**
 * @file
 * Global GUID Checker.
 */

/**
 * Implements hook_node_presave().
 */
function global_guid_checker_node_presave($node) {

  $guid_field = variable_get('global_guid_checker_guid_field', 'field_global_guid');

  if ($items = field_get_items('node', $node, $guid_field)) {

    $guid_value = $items[0]['value'];

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');

    $bundle = variable_get('global_guid_checker_bundle', '');

    if ('' !== $bundle) {
      $query->entityCondition('bundle', $bundle);
    }

    if (property_exists($node, 'original')) {
      // If we are editing a node, exclude it from the query.
      $query->propertyCondition('nid', $node->nid, '!=');
    }

    $query->fieldCondition($guid_field, 'value', $guid_value);

    $result = $query->execute();

    if (isset($result['node'])) {
      // Throw an exception to stop the node from being saved. Allow the rest
      // of the batch to continue processing. This creates lots of watchdog
      // entries which we delete in global_guid_checker_watchdog_trim(). By
      // leaving the message out of this Exception, we prevent Drupal from
      // displaying a long series of messages in the UI.
      throw new Exception();
    }

  }

}

/**
 * Implements hook_node_feeds_after_import().
 */
function global_guid_checker_feeds_after_import(FeedsSource $source) {

  if (1 === variable_get('global_guid_checker_watchdog_trim', 1)) {
    // Remove some log entries.
    global_guid_checker_watchdog_trim();
  }

}

/**
 * Remove some unuseful log entries.
 *
 * Removes log messages caused by Exceptions thrown in
 * global_guid_checker_node_presave().
 */
function global_guid_checker_watchdog_trim() {

  // Delete the feed item exceptions.
  $deleted = db_delete('watchdog')
    ->condition('referer', '%batch?op=start%', 'LIKE')
    ->condition('location', '%batch?id=%', 'LIKE')
    ->condition('type', 'node')
    ->condition('severity', 3)
    ->condition('variables', '%global_guid_checker%', 'LIKE')
    ->condition('variables', '%Exception%', 'LIKE')
    ->execute();

}
